'use strict';
// var getY = function(x, maxY, numWaves) {
//     maxY     = maxY || 25;
//     numWaves = numWaves || 0.005;

//     var pi = Math.PI;
//     var y  = (maxY / 2) + (maxY / 2) * Math.sin(pi * 2 * x * numWaves);

//     return y;
// };


var gameState = {

    preload: function() {
        // preloading boid image
        game.load.image('ground', 'sprites/ground.png');
        game.load.image('hills', 'sprites/hills.png');
        game.load.image('trees', 'sprites/trees.png');
        game.load.image('fog', 'sprites/fog.png');
        game.load.image('fog-1', 'sprites/fog-1.png');
        game.load.image('fog-2', 'sprites/fog-2.png');
        game.load.image('fog-mask', 'sprites/fog-mask.png');
        game.load.image('mountains', 'sprites/mountains.png');

        game.load.spritesheet('guy', 'sprites/guy.png', 16, 16);
        // game.load.spritesheet('guy', 'sprites/guy2.png', 12, 20);

        game.stage.backgroundColor = '#9aa7b8';
    // game.stage.backgroundColor = '#000';
    },
    create: function() {
        var wWidth  = this.world.width;
        var wHeight = this.world.height;

        var fog1   = this.fog1 = this.game.make.tileSprite(0, 0, wWidth, wHeight, 'fog-1');
        var fog2   = this.fog2 = this.game.make.tileSprite(0, 0, wWidth, wHeight, 'fog-2');
        var fogBmd = game.make.bitmapData(wWidth, wHeight);

        var moon         = this.moon = this.game.add.graphics(100, 0);
        var mountains    = this.mountains = this.add.tileSprite(0, 200, wWidth, wHeight, 'mountains');
        var fogContainer = this.fogContainer = game.add.sprite(0, 220, fogBmd);
        var trees        = this.trees = this.add.tileSprite(0, 200, wWidth, wHeight, 'trees');
        var hills        = this.hills = this.add.tileSprite(0, 200, wWidth, wHeight, 'hills');
        var ground       = this.ground = this.add.tileSprite(0, 200, wWidth, wHeight, 'ground');

        fogContainer.bmd = fogBmd;

        moon.alpha = 0.4;
        moon.beginFill(0xffffff);
        moon.drawCircle(0, 0, 100);

        var guy = this.guy = game.add.sprite(wWidth * 0.5, 480, 'guy');
        guy.anchor.set(0.5);
        guy.scale = {
            x: 2,
            y: 2
        };
        guy.animations.add('idle', [0]);
        guy.animations.add('walk', [1, 2, 3, 4, 5, 6, 93, 94], 10, true);

        this.cursors = this.game.input.keyboard.createCursorKeys();

        var fog1Time = 8000;
        fog1.tweenOffsetY = -5;
        fog1.tween = game.add.tween(fog1)
        .to({
                tweenOffsetY: 5
            },
            fog1Time,
            Phaser.Easing.Quadratic.InOut
        )
        .yoyo(true)
        .repeat(-1)
        .start();

        var fog2Time = 4000;
        fog2.tweenOffsetY = 5;
        fog2.tween = game.add.tween(fog2)
        .to({
                tweenOffsetY: -5,
            },
            fog2Time,
            Phaser.Easing.Quadratic.InOut
        )
        .yoyo(true)
        .repeat(-1)
        .start();

    },
    updateFog: function() {
        var fog1   = this.fog1;
        var fog2   = this.fog2;
        var fogBmd = this.fogContainer.bmd;

        fogBmd.clear();
        fogBmd.blendReset();

        fog1.renderToCanvas(fogBmd, 0 , fog1.tweenOffsetY);
        fogBmd.blendDestinationIn();
        fog2.renderToCanvas(fogBmd, 0, fog2.tweenOffsetY);

    },
    update: function() {

        var groundSpeed = 3;
        var hillsSpeed  = 0.75;
        var treesSpeed  = 0.5;
        var fog1Speed   = 0.125;
        var fog2Speed   = 0.2;
        // var fog1Speed      = 2;
        // var fog2Speed      = 2;
        var mountainsSpeed = 0.1;
        var moonSpeedX     = 0.1;
        var moonSpeedY     = 0.2;
        var moonSpeedA     = 0.0005;

        var guy = this.guy;

        this.fog1.tilePosition.x += fog1Speed;
        this.fog2.tilePosition.x -= fog2Speed;

        if (this.cursors.left.isDown) {
            this.ground.tilePosition.x += groundSpeed;
            this.hills.tilePosition.x += hillsSpeed;
            this.trees.tilePosition.x += treesSpeed;
            this.fog1.tilePosition.x -= fog1Speed;
            this.fog2.tilePosition.x -= fog2Speed;
            this.mountains.tilePosition.x += mountainsSpeed;

            this.moon.x -= moonSpeedX;
            this.moon.y -= moonSpeedY;
            this.moon.alpha += moonSpeedA;

            guy.animations.play('walk');
            guy.scale.x = -2;

            // this.updateFog();

        } else if (this.cursors.right.isDown) {
            this.ground.tilePosition.x -= groundSpeed;
            this.hills.tilePosition.x -= hillsSpeed;
            this.trees.tilePosition.x -= treesSpeed;
            // this.fog1.tilePosition.x -= fog1Speed;
            // this.fog2.tilePosition.x += fog2Speed;
            this.mountains.tilePosition.x -= mountainsSpeed;

            this.moon.x += moonSpeedX;
            this.moon.y += moonSpeedY;
            this.moon.alpha -= moonSpeedA;

            guy.animations.play('walk');
            guy.scale.x = 2;

            // this.updateFog();
        } else {
            guy.animations.play('idle');
        }
        this.updateFog();
    },
};

var game;

(function() {

    var width  = 800;
    var height = 600;

    game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser-example', null, false, false);
    game.state.add('Game', gameState, true);

})();

